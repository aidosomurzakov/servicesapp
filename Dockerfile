FROM node:13.12.0-alpine

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH
ENV PATH /app/.env:$PATH

COPY package.json ./
COPY package-lock.json ./
COPY . ./
RUN npm i styled-components@5 react react-dom
RUN npm install --silent
RUN npm install react-scripts@3.4.1 -g --silent


CMD ["npm", "start"]