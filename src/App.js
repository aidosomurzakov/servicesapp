import React, { Fragment } from 'react';
import { Route } from "react-router";

import './styles/global.css';

import Login from './containers/Login/Login';
import ManagePanel from './containers/AdminPanel/AdminPanel';
import CreateService from './containers/CreateService/CreateService';
import List from './containers/List/List';
import Header from './components/Header/Header';

import './App.css';

function App() {
  return (
    <Fragment>
      <header>
        <Header />
      </header>
      <main>
        <Route path="/login/sign-in" exact component={Login} />
        <Route path="/manage-panel" exact component={ManagePanel} />
        <Route path="/create/:serviceId" exact component={CreateService} />
        <Route path="/edit/:serviceId" exact component={CreateService} />
        <Route path="/list/:serviceId" exact component={List} />
      </main>
    </Fragment>
  );
}

export default App;
