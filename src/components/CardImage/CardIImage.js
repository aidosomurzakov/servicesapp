import React from 'react';
import styled from 'styled-components';

import { makeStyles } from '@material-ui/core/styles';

import { NavLink as RouterNavLink } from 'react-router-dom';

import {
    NavLink,
} from "reactstrap";

import { Card, CardActionArea, CardActions, CardContent, CardMedia, Button, Typography } from '@material-ui/core';

import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 140,
    },
});

export default function MediaCard({ avatar, title, serviceName }) {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardActionArea>
                <CardMedia
                    className={classes.media}
                    image={avatar}
                    title="Contemplative Reptile"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {title}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        lorOccaecat anim velit quis sint.Reprehenderit quis commodo nulla quis duis ipsum dolor velit voluptate aliqua aliquip in quis. Ea ipsum ut occaecat ad non tempor cillum tempor minim. Culpa magna consectetur anim velit pariatur amet qui aliquip incididunt qui consectetur.
          </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Button size="small" color="primary">
                    <CustomNavlink tag={RouterNavLink} to={`/edit/${serviceName}`} exact>
                        <EditIcon />
                    </CustomNavlink>
                </Button>
                <Button size="small" color="primary">
                    <DeleteIcon />
                </Button>

            </CardActions>
        </Card>
    );
}

const CustomNavlink = styled(NavLink)`
    padding: 0;
`;