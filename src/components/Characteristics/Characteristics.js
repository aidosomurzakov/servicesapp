import React from 'react';
import styled from 'styled-components';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';


const useStyles = makeStyles({
    margin: {
        marginRight: '0.5rem'
    },
});

const Characteristics = ({ title, byPeriod, characteristics, from, to }) => {
    const classes = useStyles();
    return (
        <Wrapper>
            {byPeriod ?
                <>
                    <ParagraphCharacteristic>{title}</ParagraphCharacteristic>
                    {characteristics.map(item => (
                        <div>
                            С <TimeCharacteristicText>{item.from}</TimeCharacteristicText> по <TimeCharacteristicText>{item.to}</TimeCharacteristicText><Text>{item.title}</Text><EditIcon className={classes.margin} /><DeleteIcon />
                        </div>
                    ))}
                    <AddIcon /><AddCharacteristicText>Добавить характеристику</AddCharacteristicText>
                </>
                : <>
                    <ParagraphCharacteristic>{title}</ParagraphCharacteristic>
                    {characteristics.map(item => (
                        <div>
                            <Text>{item}</Text><EditIcon className={classes.margin} /><DeleteIcon />
                        </div>
                    ))}
                    <AddIcon /><AddCharacteristicText>Добавить характеристику</AddCharacteristicText>
                </>}

        </Wrapper>
    )
}

export default Characteristics;

const Wrapper = styled.div`
    margin-left: 1rem;
`;

const Text = styled.span`
    margin-right: 1rem;
    margin-left: 0.9375rem;
`;

const AddCharacteristicText = styled.span`
    font-weight: 500;
    margin-top: 1rem;
    margin-bottom: 1rem;
    display: inline-block;
`;

const TimeCharacteristicText = styled.span`
    font-weight: 500;
`;
const ParagraphCharacteristic = styled.p`
    margin-left: 0.9375rem;
    color: gray;
    font-size: 1.25rem;
`;