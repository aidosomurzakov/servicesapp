import 'date-fns';
import React from 'react';
import styled from 'styled-components';

import DateRangeIcon from '@material-ui/icons/DateRange';

import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
} from '@material-ui/pickers';

import AddIcon from '@material-ui/icons/Add';

const useStyles = makeStyles({
    grid: {
        width: '100%',
    },
    margin: {
        marginRight: '2rem',
        marginLeft: '1.0625rem',
        marginBottom: '0.5rem'
    },
    color: {
        color: '#03a9f4'
    }
});

export default function MaterialUIPickers({ title, withoutTime, withTime }) {
    // The first commit of Material-UI
    const [selectedDate, setSelectedDate] = React.useState(new Date());

    const classes = useStyles();

    function handleDateChange(date) {
        setSelectedDate(date);
    }

    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            {title && <Title><DateRangeIcon /><span>{title}</span></Title>}
            <Container>
                <Grid container className={classes.grid, classes.margin} justify="space-around" lg={2}>
                    Start
                    {!withTime ?
                        <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            className={classes.color}
                            format="MM/dd/yyyy"
                            margin="normal"
                            id="date-picker-inline"
                            value={selectedDate}
                            onChange={handleDateChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                        : null}

                    {!withoutTime ? <KeyboardTimePicker
                        margin="normal"
                        id="mui-pickers-time"
                        value={selectedDate}
                        onChange={handleDateChange}
                        KeyboardButtonProps={{
                            'aria-label': 'change time',
                        }}
                        ampm
                    /> : null}

                </Grid>
                <Grid container className={classes.grid, classes.margin} justify="space-around" lg={2}>
                    End
                    {!withTime ?
                        <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            format="MM/dd/yyyy"
                            margin="normal"
                            id="date-picker-inline"
                            value={selectedDate}
                            onChange={handleDateChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        /> : null}
                    {!withoutTime ? <KeyboardTimePicker
                        margin="normal"
                        id="mui-pickers-time"
                        value={selectedDate}
                        onChange={handleDateChange}
                        KeyboardButtonProps={{
                            'aria-label': 'change time',
                        }}
                        ampm
                    /> : null}

                </Grid>
                {!withoutTime ? <>
                    <AddIcon />Add period</>
                    : null}

            </Container>

        </MuiPickersUtilsProvider>
    );
}

const Container = styled.div`
    display: flex;
`;

const Title = styled.h1`
    font-size: 1.25rem;
    color: #03a9f4;
    margin-left: 1.0625rem;
    margin-top: 0.875rem;
    span {
        margin-left: 0.5rem;
        vertical-align: middle;
    }
`;