import React from 'react';
import styled from 'styled-components';

import UploadButton from '../Ui/Uploadbutton/UploadButton';

const LogoPhoto = () => (
    <Container>
        <div>
            <Title>Логотип</Title>
            <CustomParagraph>для исопльзования в интерфейсе системы</CustomParagraph>
            <CustomParagraph>загружено</CustomParagraph>
            <Title>Логотип</Title>
            <CustomParagraph>для печати на билетах</CustomParagraph>
        </div>
        <div>
            <UploadButton />
            <p>ФОТО</p>
            <p>Формат файла</p>
            <p>Радио Баттен</p>
            <button disabled>Загрузить файл</button>
        </div>
    </Container>
);

export default LogoPhoto;

const Container = styled.div`
    display: flex;
`;

const Title = styled.h1`
    font-size: 1.25rem;
    color: #03a9f4;
    margin-left: 1.0625rem;
`;

const CustomParagraph = styled.p`
    font-size: 0.625rem;
    color: #03a9f4;
    margin-left: 1.0625rem;
`;
