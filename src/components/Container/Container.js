import React from 'react';
import styled from 'styled-components';

const Container = ({ className, children }) => (
    <CustomContainer className={className}>
        {children}
    </CustomContainer>
);

export default Container;

const CustomContainer = styled.div`
    width: 1024px;
    margin: 0 auto;
`;
