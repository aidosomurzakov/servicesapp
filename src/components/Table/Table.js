import React from 'react';

import { NavLink as RouterNavLink } from 'react-router-dom';

import {
    NavLink,
} from "reactstrap";

import { makeStyles, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@material-ui/core';
import FilterList from '@material-ui/icons/FilterList';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
    tableCell: {
        color: '#03a9f4',
    },
});

function createData(abonement, administrator, service) {
    return { abonement, administrator, service };
}

const rows = [
    createData('Абонемент 1', 'Адимнистратор 1', 'Услуга 1'),
    createData('Абонемент 2', 'Адимнистратор 2', 'Услуга 2'),
    createData('Абонемент 3', 'Адимнистратор 3', 'Услуга 3'),
];

export default function SimpleTable({ serviceName }) {
    const classes = useStyles();

    return (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell className={classes.tableCell}>Наименование абонента</TableCell>
                        <TableCell className={classes.tableCell} align="right">Администратор абонента</TableCell>
                        <TableCell className={classes.tableCell} align="right">Вид услуги</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row.name}>
                            <TableCell component="th" scope="row">
                                {row.abonement}
                            </TableCell>
                            <TableCell align="right">{row.administrator}</TableCell>
                            <TableCell align="right">{row.service}</TableCell>
                            <TableCell align="right">
                                <NavLink tag={RouterNavLink} to={`/edit/${serviceName}`} exact>
                                    <EditIcon />
                                </NavLink></TableCell>
                            <TableCell align="right"><DeleteIcon /></TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

