import React from 'react';
import styled from 'styled-components';

import { makeStyles } from '@material-ui/core/styles';

import { NavLink as RouterNavLink } from 'react-router-dom';

import {
    Nav,
    Navbar,
    NavbarBrand,
    NavItem,
    NavLink,
} from "reactstrap";

import PersonIcon from '@material-ui/icons/Person';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { FormControl } from '@material-ui/core';
import NativeSelect from '@material-ui/core/NativeSelect';



const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    whiteColor: {
        color: '#fff',

    },
    background: {
        backgroundColor: 'rgba(13,98,253,0.8);',
    },
}));

const Header = () => {
    const classes = useStyles();
    return (
        <HeaderWrapper>
            <Container>
                <div>Admin Panel</div>
                <LogoWrapper>
                    <FormControl className={classes.formControl}>
                        <NativeSelect
                            className={classes.whiteColor}
                            defaultValue={30}
                            inputProps={{
                                name: 'name',
                                id: 'uncontrolled-native',
                            }}
                        >
                            <CustomOption value={10}>Рус</CustomOption>
                            <CustomOption value={20}>Қаз</CustomOption>
                            <CustomOption value={30}>Eng</CustomOption>
                        </NativeSelect>
                    </FormControl>
                    <NotificationsIcon />
                    <CustomNavlink tag={RouterNavLink} to="/login/sign-in" exact><PersonIcon /></CustomNavlink>
                </LogoWrapper>
            </Container>
        </HeaderWrapper>
    );
};

export default Header;

const HeaderWrapper = styled.div`
    display: flex;
    align-items: center;
    color: white;
    height: 3.75rem;
    background: rgba(13,98,253,0.8);
`;

const CustomNavlink = styled(NavLink)`
    background-color: inherit;
    color: white;
    padding: 0;
`;

const LogoWrapper = styled.div`
    display: flex;
    align-items: center;
`;

const Container = styled.div`
    width: 1024px;
    margin: 0 auto;
    display: flex;
    align-items: center;
    justify-content: space-between;
    ul,svg{
        margin-right: 1rem;
    }
`;

const CustomOption = styled.option`
    background-color: rgba(13,98,253,0.8) !important;
`;
