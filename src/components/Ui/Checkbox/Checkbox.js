import React from 'react';
import clsx from 'clsx';
import styled from 'styled-components';

import { makeStyles, withStyles } from '@material-ui/core/styles';

import { Radio, RadioGroup, FormControlLabel, FormControl, TextField, FormGroup, Checkbox } from '@material-ui/core';

import DatePicker from '../../DatePicker/DatePicker';
import RadioButtonsGroup from '../RadioButton/RadioButton';

import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';


const CssTextField = withStyles({
    root: {
        '& label.Mui-focused': {
            color: 'green',
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: 'green',
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: '#03a9f4',
            },
            '&:hover fieldset': {
                borderColor: '#03a9f4',
            },
            '&.Mui-focused fieldset': {
                borderColor: '#03a9f4',
            },
            backgroundColor: '#c3e0ff',
            borderRadius: '10px',
        },
    },
})(TextField);

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    block: {
        display: 'block',
        marginLeft: '0.5rem'
    },
    margin: {
        margin: theme.spacing(1),
    },
    withoutLabel: {
        marginTop: theme.spacing(3),
    },
    textField: {
        width: '10ch',
    },
}));

const MyCheckBox = ({ title, serivce, labels, rowType, hourCheckbox, weekCheckbox, byPeriodAttendance, byAttendance }) => {

    const classes = useStyles();
    const [value, setValue] = React.useState('');
    const [state, setState] = React.useState({
        byWeekDays: false,
        monday: false,
        tuesday: false,
        wensday: false,
        thursday: false,
        friday: false,
        saturday: false,
        sunday: false,
        periodAttendance: false,
        attendance: false
    });

    const handleChange = (event) => {
        setValue(event.target.value);
    };

    const handleChangeWeek = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
    };
    const { byWeekDays, monday, tuesday, wensday, thursday, friday, saturday, sunday, periodAttendance, attendance } = state;

    return (
        <Container serivce={serivce}>
            {hourCheckbox && <>
                {title && <Title>{title}</Title>}
                <FormControl component="fieldset">
                    <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChange} className={rowType ? classes.block : null}>
                        {labels.map(item => (<FormControlLabel value={item.value} control={<Radio icon={<CheckBoxOutlineBlankIcon />} checkedIcon={<CheckBoxIcon />} style={{ color: '#03a9f4' }} />} label={item.title} />))}
                    </RadioGroup>
                </FormControl>
                {value === 'lim' ? <>
                    <DatePicker withTime />
                </> : null}
            </>}
            {weekCheckbox && <>
                <FormControl component="fieldset" className={classes.formControl}>
                    <FormGroup>
                        <FormControlLabel
                            control={<Checkbox style={{ color: '#03a9f4' }} checked={byWeekDays} onChange={handleChangeWeek} name="byWeekDays" />}
                            label="По дням недели"
                        />
                    </FormGroup>

                </FormControl>
                {byWeekDays ?
                    <Weekend>
                        <FormControl component="fieldset" className={classes.formControl}>
                            <FormGroup>
                                <FormControlLabel
                                    control={<Checkbox style={{ color: '#03a9f4' }} checked={monday} onChange={handleChangeWeek} name={"monday"} />}
                                    label={'Пн'}

                                />
                                <FormControlLabel
                                    control={<Checkbox style={{ color: '#03a9f4' }} checked={tuesday} onChange={handleChangeWeek} name={"tuesday"} />}
                                    label={'Вт'}

                                />
                                <FormControlLabel
                                    control={<Checkbox style={{ color: '#03a9f4' }} checked={wensday} onChange={handleChangeWeek} name={"wensday"} />}
                                    label={'Ср'}

                                />
                                <FormControlLabel
                                    control={<Checkbox style={{ color: '#03a9f4' }} checked={thursday} onChange={handleChangeWeek} name={"thursday"} />}
                                    label={'Чт'}

                                />
                                <FormControlLabel
                                    control={<Checkbox style={{ color: '#03a9f4' }} checked={friday} onChange={handleChangeWeek} name={"friday"} />}
                                    label={'Пт'}

                                />
                                <FormControlLabel
                                    control={<Checkbox style={{ color: '#03a9f4' }} checked={saturday} onChange={handleChangeWeek} name={"saturday"} />}
                                    label={'Сб'}

                                />
                                <FormControlLabel
                                    control={<Checkbox style={{ color: '#03a9f4' }} checked={sunday} onChange={handleChangeWeek} name={"sunday"} />}
                                    label={'Вс'}

                                />
                            </FormGroup>
                        </FormControl>
                    </Weekend> : null}
            </>}
            {byPeriodAttendance ?
                <>
                    <FormControl component="fieldset" className={classes.formControl}>
                        <FormGroup>
                            <FormControlLabel
                                control={<Checkbox style={{ color: '#03a9f4' }} checked={periodAttendance} onChange={handleChangeWeek} name="periodAttendance" />}
                                label="По количествам посещений за период"
                            />
                        </FormGroup>
                    </FormControl>
                    {periodAttendance &&
                        <Wrapper>
                            <div>
                                <FormControl className={clsx(classes.textField)} variant={'outlined'}>
                                    <CssTextField
                                        className={classes.margin}
                                        id="outlined-adornment-weight"
                                        variant={'outlined'}
                                        // value={values.weight}
                                        // onChange={handleChange('weight')}
                                        aria-describedby="outlined-weight-helper-text"
                                        inputProps={{
                                            'aria-label': 'weight',
                                        }}

                                    />
                                </FormControl>
                            </div>
                            <div>
                                <CustomText> раза в</CustomText>
                            </div>
                            <div>
                                <RadioButtonsGroup labels={['Месяц', 'Неделя', 'Год']} rowType />
                            </div>
                        </Wrapper>}

                </>
                : null}
            {byAttendance &&
                <>
                    <FormControl component="fieldset" className={classes.formControl}>
                        <FormGroup>
                            <FormControlLabel
                                control={<Checkbox style={{ color: '#03a9f4' }} checked={attendance} onChange={handleChangeWeek} name="attendance" />}
                                label="По количествам посещений"
                            />
                        </FormGroup>
                    </FormControl>
                    {attendance &&
                        <Wrapper>
                            <div>
                                <FormControl className={clsx(classes.textField)} variant={'outlined'}>
                                    <CssTextField
                                        className={classes.margin}
                                        id="outlined-adornment-weight"
                                        variant={'outlined'}
                                        // value={values.weight}
                                        // onChange={handleChange('weight')}
                                        aria-describedby="outlined-weight-helper-text"
                                        inputProps={{
                                            'aria-label': 'weight',
                                        }}

                                    />
                                </FormControl>
                            </div>
                        </Wrapper>}
                </>


            }

        </Container>

    );
}


export default MyCheckBox;

const Title = styled.h1`
    font-size: 1.25rem;
    color: #03a9f4;
`;

const Container = styled.div`
   margin-left:${(props) => (props.serivce ? `1rem` : '0')};
`;

const Weekend = styled.div`
    margin-left: 1rem;
`;

const Wrapper = styled.div`
    display: flex;
    align-items: center;
`;

const CustomText = styled.span`
    display: inline-block;
    margin-bottom: 0.5625rem;
`;