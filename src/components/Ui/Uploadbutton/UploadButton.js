import React from 'react';
import { connect } from 'react-redux';

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { saveImage } from '../../../store/actions/actions';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    input: {
        display: 'none',
    },
}));

const UploadButton = ({ onSaveImage }) => {
    const classes = useStyles();
    const fileSelectedHandler = (event) => {
        onSaveImage(event.target.files[0])
    };
    return (
        <div className={classes.root}>
            <input
                accept="image/*"
                className={classes.input}
                id="contained-button-file"
                multiple
                type="file"
                onChange={fileSelectedHandler}
            />
            <label htmlFor="contained-button-file">
                <Button variant="contained" color="primary" component="span">
                    Upload
        </Button>
            </label>
        </div>
    );
};

const mapDispatchToProps = {
    onSaveImage: (img) => saveImage(img),
};

export default connect(null, mapDispatchToProps)(UploadButton);
