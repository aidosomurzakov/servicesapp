import React from 'react';

import styled from 'styled-components';

import { makeStyles } from '@material-ui/core/styles';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';

const useStyles = makeStyles((theme) => ({
    block: {
        display: 'block',
        marginLeft: '0.5rem'
    },
}));
const RadioButtonsGroup = ({ title, serivce, labels, rowType }) => {
    const classes = useStyles();
    const [value, setValue] = React.useState('');

    const handleChange = (event) => {
        setValue(event.target.value);
    };

    return (
        <Container serivce={serivce}>
            {title && <Title>{title}</Title>}
            <FormControl component="fieldset">
                <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChange} className={rowType ? classes.block : null}>
                    {labels.map(item => (<FormControlLabel value={item} control={<Radio style={{ color: '#03a9f4' }} />} label={item} />))}
                </RadioGroup>
            </FormControl>
        </Container>

    );
}


export default RadioButtonsGroup;

const Title = styled.h1`
    font-size: 1.25rem;
    color: #03a9f4;
`;

const Container = styled.div`
   margin-left:${(props) => (props.serivce ? `1rem` : '0')};
`;
