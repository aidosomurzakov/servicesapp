import React from 'react';
import styled from 'styled-components';

import { makeStyles, withStyles } from '@material-ui/core/styles';

import { purple } from '@material-ui/core/colors';

import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';

import Form from '../Form/Form';

const useStyles = makeStyles({
    margin: {
        margin: '1rem 0 1rem 1.0625rem'
    },
    flex: {
        display: 'flex',
    }
});


const PurpleSwitch = withStyles({
    switchBase: {
        color: "#03a9f4",
        '&$checked': {
            color: "#03a9f4",
        },
        '&$checked + $track': {
            backgroundColor: "#03a9f4",
        },
    },
    checked: {},
    track: {},
})(Switch);

export default function SwitchesGroup({ service, abonements, title }) {
    const classes = useStyles();
    const [state, setState] = React.useState({
        allCategory: true,
        category_1: false,
        category_2: true,
    });

    const handleChange = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
    };

    return (
        <FormControl component="fieldset" className={classes.margin} >
            {service ?
                <FormGroup>
                    <Grid container spacing={1} lg={5}>
                        <Grid item xs={6} className={classes.flex}>
                            <FormControlLabel
                                control={<PurpleSwitch checked={state.gilad} onChange={handleChange} name="allCategory" />}
                                label="Все категории"
                                labelPlacement="start"
                            />
                        </Grid>
                        <Grid item xs={6} >
                            <Form size="10ch" additionalLabel={"Все категории"} />
                        </Grid>
                        <Grid item xs={6} className={classes.flex}>
                            <FormControlLabel
                                control={<PurpleSwitch checked={state.jason} onChange={handleChange} name="category_1" />}
                                label="Категория 1"
                                labelPlacement="start"
                            />
                        </Grid>
                        <Grid item xs={6} className={classes.flex}>
                            <Form size="10ch" additionalLabel={"Категория 1"} />
                        </Grid>
                        <Grid item xs={6} className={classes.flex}>
                            <FormControlLabel
                                control={<PurpleSwitch checked={state.antoine} onChange={handleChange} name="category_2" />}
                                label="Категория 2"
                                labelPlacement="start"
                            />
                        </Grid>
                        <Grid item xs={6} className={classes.flex}>
                            <Form size="10ch" additionalLabel={"Категория 2"} />
                        </Grid>
                    </Grid>
                </FormGroup>
                : null}
            {abonements ?
                <Wrapper>
                    <Title>{title}</Title>
                    <FormGroup>
                        <FormControlLabel
                            control={<PurpleSwitch checked={state.gilad} onChange={handleChange} name="allCategory" />}
                            label="Все категории"
                            labelPlacement="start"
                        />
                        <FormControlLabel
                            control={<PurpleSwitch checked={state.jason} onChange={handleChange} name="category_1" />}
                            label="Категория 1"
                            labelPlacement="start"
                        />
                        <FormControlLabel
                            control={<PurpleSwitch checked={state.antoine} onChange={handleChange} name="category_2" />}
                            label="Категория 2"
                            labelPlacement="start"
                        />
                    </FormGroup>
                </Wrapper>
                : null}

            {/* <Title>Категории ползьователей</Title>
            <Title>Стоимость</Title> */}
        </FormControl>
    );
}

const Title = styled.h1`
    font-size: 1.25rem;
    color: #03a9f4;
`;

const Wrapper = styled.div`
    display: flex;
    align-items: flex-start;
    flex-direction: column;
    label {
        margin-left: 0;
    }
`;