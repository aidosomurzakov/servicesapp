import React from 'react';
import clsx from 'clsx';
import styled from 'styled-components';

import { makeStyles, withStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';

import RadioButtonsGroup from '../RadioButton/RadioButton';

const CssTextField = withStyles({
    root: {
        '& label.Mui-focused': {
            color: 'green',
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: 'green',
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: '#03a9f4',
            },
            '&:hover fieldset': {
                borderColor: '#03a9f4',
            },
            '&.Mui-focused fieldset': {
                borderColor: '#03a9f4',
            },
            backgroundColor: '#c3e0ff',
            borderRadius: '10px',
        },
    },
})(TextField);

const currencies = [
    {
        value: 'USD',
        label: '$',
    },
    {
        value: 'EUR',
        label: '€',
    },
    {
        value: 'BTC',
        label: '฿',
    },
    {
        value: 'JPY',
        label: '¥',
    },
];

const Form = ({
    title, secondTitle, size, multiline, rows, defaultValue, variant, fullWidth, label, oneCollumn, twoCollumn, select, additionalLabel, conditional, conditionalText, abonement, endPeriod, period, startPeriod
}) => {
    const useStyles = makeStyles((theme) => ({
        root: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        margin: {
            margin: theme.spacing(1),
        },
        withoutLabel: {
            marginTop: theme.spacing(3),
        },
        textField: {
            width: size || '50ch',
        },
    }));
    const abonementLabels = ['Дней', 'Часов'];

    const classes = useStyles();
    const [values, setValues] = React.useState({
        weight: '',
    });
    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };
    return (
        <>
            {oneCollumn ? (
                <>
                    <Title>{title}</Title>
                    <div>
                        <FormControl className={clsx(classes.margin, classes.textField)} variant={variant || 'outlined'}>
                            <CssTextField
                                className={classes.margin}
                                id="outlined-adornment-weight"
                                variant={variant || 'outlined'}
                                value={values.weight}
                                onChange={handleChange('weight')}
                                aria-describedby="outlined-weight-helper-text"
                                multiline={multiline || false}
                                select={select || false}
                                label={label || ''}
                                rows={rows || 0}
                                inputProps={{
                                    'aria-label': 'weight',
                                }}
                                SelectProps={select ? {
                                    native: true,
                                } : null}
                                fullWidth={fullWidth || true}
                            >
                                {select && currencies.map((option) => (
                                    <option key={option.value} value={option.value}>
                                        {option.label}
                                    </option>
                                ))}
                            </CssTextField>
                        </FormControl>
                    </div>
                </>
            ) : null}
            {twoCollumn ? (
                <Wrapper>
                    <div>
                        <Title>{title}</Title>
                        <FormControl className={clsx(classes.margin, classes.textField)} variant={variant || 'outlined'}>
                            <CssTextField
                                className={classes.margin}
                                id="outlined-adornment-weight"
                                variant={variant || 'outlined'}
                                value={values.weight}
                                onChange={handleChange('weight')}
                                aria-describedby="outlined-weight-helper-text"
                                multiline={multiline || false}
                                label={label || ''}
                                rows={rows || 0}
                                inputProps={{
                                    'aria-label': 'weight',
                                }}
                                fullWidth={fullWidth || true}
                            />
                        </FormControl>
                    </div>
                    <div>
                        <Title>{secondTitle}</Title>
                        <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                            <CssTextField
                                className={classes.margin}
                                id="outlined-select-currency-native"
                                select={select || false}
                                // value={currency}
                                onChange={handleChange}
                                SelectProps={{
                                    native: true,
                                }}
                                variant="outlined"
                            >
                                {select && currencies.map((option) => (
                                    <option key={option.value} value={option.value}>
                                        {option.label}
                                    </option>
                                ))}
                            </CssTextField>
                        </FormControl>
                    </div>

                </Wrapper>
            )
                : null}
            {additionalLabel ?
                <AdditionalLabelWrapper>
                    <AdditionalLabel>{additionalLabel}</AdditionalLabel>
                    <FormControl className={clsx(classes.margin, classes.textField)} variant={variant || 'outlined'}>
                        <CssTextField
                            className={classes.margin}
                            id="outlined-adornment-weight"
                            variant={variant || 'outlined'}
                            value={values.weight}
                            onChange={handleChange('weight')}
                            aria-describedby="outlined-weight-helper-text"
                            multiline={multiline || false}
                            select={select || false}
                            label={label || ''}
                            rows={rows || 0}
                            inputProps={{
                                'aria-label': 'weight',
                            }}
                            fullWidth={fullWidth || true}
                        />
                    </FormControl>
                </AdditionalLabelWrapper> : null}
            {conditional ? (
                <>
                    <Title>{title}</Title>
                    <Wrapper>

                        <div>
                            <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                                <CssTextField
                                    className={classes.margin}
                                    id="outlined-select-currency-native"
                                    select={select || false}
                                    // value={currency}
                                    onChange={handleChange}
                                    SelectProps={{
                                        native: true,
                                    }}
                                    variant="outlined"
                                >
                                    {currencies.map((option) => (
                                        <option key={option.value} value={option.value}>
                                            {option.label}
                                        </option>
                                    ))}
                                </CssTextField>
                            </FormControl>
                        </div>
                        <ConditionalText>{conditionalText}</ConditionalText>
                        <div>
                            <FormControl className={clsx(classes.margin, classes.textField)} variant={variant || 'outlined'}>
                                <CssTextField
                                    className={classes.margin}
                                    id="outlined-adornment-weight"
                                    variant={variant || 'outlined'}
                                    value={values.weight}
                                    onChange={handleChange('weight')}
                                    aria-describedby="outlined-weight-helper-text"
                                    multiline={multiline || false}
                                    label={label || ''}
                                    rows={rows || 0}
                                    inputProps={{
                                        'aria-label': 'weight',
                                    }}
                                    fullWidth={fullWidth || true}
                                />
                            </FormControl>
                        </div>

                    </Wrapper>
                </>
            ) : null}
            {abonement ? (
                <>
                    {period ? <>
                        <Title>{title}</Title>
                        <Wrapper>
                            <div>
                                <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                                    <CssTextField
                                        className={classes.margin}
                                        id="outlined-adornment-weight"
                                        variant={variant || 'outlined'}
                                        // value={values.weight}
                                        // onChange={handleChange('weight')}
                                        aria-describedby="outlined-weight-helper-text"
                                        multiline={multiline || false}
                                        label={label || ''}
                                        rows={rows || 0}
                                        inputProps={{
                                            'aria-label': 'weight',
                                        }}
                                    />
                                </FormControl>
                            </div>
                            <div>
                                <RadioButtonsGroup labels={abonementLabels} rowType />
                            </div>
                        </Wrapper>
                    </> : null}
                    {startPeriod ? <>
                        <Title>{title}</Title>
                        <Wrapper>
                            <Title>С</Title>
                            <div>
                                <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                                    <CssTextField
                                        className={classes.margin}
                                        id="outlined-adornment-weight"
                                        variant={variant || 'outlined'}
                                        // value={values.weight}
                                        // onChange={handleChange('weight')}
                                        aria-describedby="outlined-weight-helper-text"
                                        multiline={multiline || false}
                                        label={label || ''}
                                        rows={rows || 0}
                                        inputProps={{
                                            'aria-label': 'weight',
                                        }}
                                    />
                                </FormControl>
                            </div>
                            <ConditionalText>{conditionalText}</ConditionalText>
                            <div>
                                <RadioButtonsGroup labels={['С момента приобритения']} rowType />
                            </div>
                        </Wrapper>
                    </> : null}
                    {endPeriod ? <>
                        <Title>{title}</Title>
                        <Wrapper>
                            <Title>До</Title>
                            <div>
                                <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                                    <CssTextField
                                        className={classes.margin}
                                        id="outlined-adornment-weight"
                                        variant={variant || 'outlined'}
                                        // value={values.weight}
                                        // onChange={handleChange('weight')}
                                        aria-describedby="outlined-weight-helper-text"
                                        multiline={multiline || false}
                                        label={label || ''}
                                        rows={rows || 0}
                                        inputProps={{
                                            'aria-label': 'weight',
                                        }}

                                    />
                                </FormControl>
                            </div>
                            <ConditionalText>{conditionalText}</ConditionalText>
                            <div>
                                <FormControl className={clsx(classes.margin, classes.textField)} variant={variant || 'outlined'}>
                                    <CssTextField
                                        className={classes.margin}
                                        id="outlined-adornment-weight"
                                        variant={variant || 'outlined'}
                                        value={values.weight}
                                        onChange={handleChange('weight')}
                                        aria-describedby="outlined-weight-helper-text"
                                        multiline={multiline || false}
                                        label={label || ''}
                                        rows={rows || 0}
                                        inputProps={{
                                            'aria-label': 'weight',
                                        }}

                                    />
                                </FormControl>
                            </div>
                            <div>
                                <RadioButtonsGroup labels={abonementLabels} rowType />
                            </div>
                        </Wrapper>
                    </> : null}

                </>
            ) : null}
        </>

    );
};

export default Form;

const Title = styled.h1`
    font-size: 1.25rem;
    color: #03a9f4;
    margin-left: 1.0625rem;
    margin-bottom: 0;
`;

const Wrapper = styled.div`
    display: flex;
    align-items: center;
`;

const AdditionalLabel = styled.h1`
    font-size: 1rem;
`;

const AdditionalLabelWrapper = styled.div`
    display: flex;
    align-items: center;
`;

const ConditionalText = styled.p`
    max-width: 8%;
    text-align: center;
`;
