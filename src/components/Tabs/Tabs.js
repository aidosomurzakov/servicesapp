import React, { FunctionComponent, ReactNode, useState } from 'react';
import styled, { css } from 'styled-components';
import media from 'styled-media-query';

import Button from '@material-ui/core/Button';

const Tabs = ({
    items,
    centered,
    onChange,
    buttonSize,
    children,
    maxWidth,
    fullWidth,
    disabled,
}) => {
    const [selectedTab, setSelectedTab] = useState(0);

    const handleClick = (i) => {
        if (typeof onChange === 'function') {
            onChange(i);
        }
        console.log(i)
        setSelectedTab(i);
        // console.log(selectedTab)
    };

    return (
        <>
            <TabContainer fullWidth={fullWidth}>
                <TabNav centered={centered} maxWidth={maxWidth}>
                    {items.map((item, i) => (
                        <TabNavButton
                            size={buttonSize}
                            active={selectedTab === i}
                            className={selectedTab === i ? 'active' : ''}
                            disabled={i === disabled}
                            key={i}
                            onClick={e => handleClick(i)}>
                            {item}
                        </TabNavButton>
                    ))}
                </TabNav>
                <TabContent>
                    {Array.isArray(children) ? children.map((data, i) => i === selectedTab && data) : children}
                </TabContent>
            </TabContainer>
        </>
    );
};

Tabs.defaultProps = {
    centered: false,
    items: [],
    onChange: i => undefined,
};

export default Tabs;

export const TabContainer = styled.div`
  --tabs-bg-color: red;
  --tabs-color: blue;
  --tabs-margin-bottom: 1.5rem;
  --tabs-padding-left: 0;
  --tabs-margin-top: 0;
  --tabs-max-width: 100%;

  position: relative;
  grid-area: tabs;

  ${props =>
        props.fullWidth &&
        css`
      ${media.lessThan('medium')`
      margin: 0 -1rem;
      padding: 0 1rem;
      overflow: hidden;
    `}
    `}
`;

const TabNavButton = styled(Button)`
  background-color: white;
  color: rgba(13,98,253,0.8);

  border-radius: 0;
  height: 3.5rem;

  ${media.lessThan('medium')`
    padding: 10px;
    min-width: 64%;
    height: 40px;
  `}

  ${props =>
        props.size === undefined &&
        css`
      ${media.greaterThan('medium')`
        flex: 1 1 0px;
      `}
    `}

  &:first-child {
    border-radius: 0.25rem 0 0 0.25rem;
  }

  &:last-child {
    border-radius: 0 0.25rem 0.25rem 0;
  }

  &:not(:last-child) {
    margin-right: 0.25rem;
  }

  &.active,
  &:hover {
    background-color: rgba(13,98,253,0.8);
    color: white;
    border-color: rgba(13,98,253,0.8);
  }
`;

const TabNav = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding-left: var(--tabs-padding-left);
  overflow-x: scroll;
  scrollbar-width: none;
  margin: 10px auto 10px auto;

  ${media.greaterThan('medium')`
    max-width: ${props => (props.maxWidth ? props.maxWidth : 'auto')};
  `}

  &::-webkit-scrollbar {
    display: none;
  }

  ${media.lessThan('medium')`
    --tabs-margin-bottom: 1rem;
    justify-content: flex-start;
    margin: 10px -1rem 10px -1rem;
    padding: 0 1rem;
  `}

  ${props =>
        props.centered &&
        css`
      justify-content: center;
    `};
`;

const TabContent = styled.div`
  filter: var(--img-filter) !important;
`;
