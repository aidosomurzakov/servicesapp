import React from 'react';
import styled, { css } from 'styled-components';
import media from 'styled-media-query';

const Circle = ({ percent, number }) => (
    <Box percent={percent}>
        <h2 className="text">Services perfomed №{number}</h2>
        <div className="percent">
            <svg>
                <circle cx="70" cy="70" r="70" />
                <circle cx="70" cy="70" r="70" />
            </svg>
            <div className="number">
                <h2>
                    {percent}
                    <span>%</span>
                </h2>
            </div>
        </div>
    </Box>
);

export default Circle;

const Box = styled.div`
  position: relative;
  height: 18.75rem;
  width: 25rem;
  @media(max-width: 1366px){
    width: auto;
    height: auto;
  }
  ${media.lessThan('1366px')`
    width: auto;
    height: auto;
 `}
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  background: #fff;
  border: 1px solid gray;
  .percent {
      position: relative;
      width: 9.375rem;
      height: 9.375rem;
  }

  .percent svg {
      position: relative;
      width: 9.375rem;
      height: 9.375rem;
  }

  .percent svg circle {
      width: 9.375rem;
      height: 9.375rem;
      fill: none;
      stroke: #000;
      stroke-width: 10;
      transform: translate(5px, 5px);
      stroke-dasharray: 440;
      stroke-dashoffset: 440;
      stroke-linecap: round;
  }

  .percent svg circle:nth-child(1) {
      stroke-dashoffset: 0;
      stroke: #cbdeff;
  }

  .percent svg circle:nth-child(2) {
      stroke-dashoffset: calc(440 - (440 * ${(props) => (props.percent ? `${props.percent}` : '0')}) / 100);
      stroke: #03a9f4;
    }   
}

  .percent .number {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
      color: #999;
  }

  .percent .number h2 {
      font-size: 3rem;
  }

  .percent .number h2 span {
      font-size: 1.5rem;
  }

  .text {
      color: #03a9f4;
      font-weight: 700;
      letter-spacing: 1px;
      margin-bottom: 2rem;
  }
`;
