import React from 'react';
import styled from 'styled-components';

import { makeStyles } from '@material-ui/core/styles';

import SearchIcon from '@material-ui/icons/Search';

import { TextField, Button, InputAdornment } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';

import RadioButton from '../Ui/RadioButton/RadioButton';

const useStyles = makeStyles((theme) => ({
    root: {
        margin: '1rem 1rem 1rem 0',
        width: "75%",
    },
    btn: {
        borderColor: '#03a9f4',
        color: '#03a9f4',
        '&:hover': {
            backgroundColor: '#bceaff'
        },
        padding: '0.5rem',

    },
    spacing: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    cssOutlinedInput: {
        '&$cssFocused $notchedOutline': {
            borderColor: `${theme.palette.primary.main} !important`,
        }
    },
    notchedOutline: {
        borderColor: '#03a9f4',
        '&:hover': {
            borderColor: '#03a9f4'
        },
    },
    cssFocused: {},
}));

export default function ComboBox({ arr }) {
    const classes = useStyles();
    const labels = ['Ascending', 'Вescending'];
    return (
        <>
            <Autocomplete
                id="combo-box-demo"
                className={classes.root}
                options={arr}
                fullWidth
                getOptionLabel={(option) => option.title}
                renderInput={(params) => <TextField {...params} variant="outlined" InputProps={{
                    ...params.InputProps,
                    startAdornment: <InputAdornment position="start"><SearchIcon /></InputAdornment>,
                    classes: {
                        root: classes.cssOutlinedInput,
                        focused: classes.cssFocused,
                        notchedOutline: classes.notchedOutline,
                    },
                }} />}
            />
            <Wrapper>
                <span>Sort by</span>
                <div className={classes.spacing}>
                    <Button className={classes.btn} variant="outlined">name</Button>
                    <Button className={classes.btn} variant="outlined">Property 1</Button>
                    <Button className={classes.btn} variant="outlined">Property 2</Button>
                </div>

                <RadioButton serivce={true} labels={labels} rowType />
            </Wrapper>
        </>
    );
}

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  max-width: 75%;
`;