import React, { FunctionComponent } from 'react';
import styled from 'styled-components';

import { NavLink as RouterNavLink } from 'react-router-dom';

import {
  NavLink,
} from "reactstrap";

import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';

const Card: FunctionComponent<CardProps> = ({
  type, title, serviceName, item,
}) => (
    <>
      {
        type === 'service'
        && (
          <FirstSection>
            <h1>{title}</h1>
            <ImageWrapper>
              <CustomNavlink tag={RouterNavLink} to={`/create/${serviceName}`} exact>
                <AddIcon fontSize="large" />
              </CustomNavlink>
              <CustomNavlink tag={RouterNavLink} to={`/list/${serviceName}`} exact>
                <EditIcon />
              </CustomNavlink>
            </ImageWrapper>
          </FirstSection>
        )
      }
      {
        type === 'report'
        && (
          <SecondSection>
            <h1>{item.title}</h1>
            <p>{item.amount}</p>
          </SecondSection>
        )

      }
    </>
  );

export default Card;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  box-sizing: border-box;
  padding: 1rem;
  height: 10rem;
  width: 18rem;
  overflow: hidden;

  h1, a {
    font-size: 1.5rem;
    font-weight: 500;
  }

  h1 {
      max-width: 20rem;
  }
`;

const FirstSection = styled(Container)`
  background-color: rgba(13,98,253,0.8);
  color: #fff;
  h1, a {
    color: white;
  }

`;

const SecondSection = styled(Container)`
  background-color: #fff;
  border: 1px solid black;
  h1 {
    color: #03a9f4;
    font-weight: 500;
  }
  p {
    font-size: 2rem;
    font-weight: 500;
  }
`;

const ImageWrapper = styled.div`
  display: flex;
  align-items: center;
  img {
      margin-right: 5px;
  }
`;

const CustomNavlink = styled(NavLink)`
    background-color: inherit;
    color: white;
    padding: 0;
`;
