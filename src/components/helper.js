export const manageList = [{ title: 'Objects', value: 'object' }, { title: 'Services', value: 'service' }, { title: 'Club Carts', value: 'club_cards' }, { title: 'Schedule', value: 'schedule' }, { title: 'Agents', value: 'agents' }, { title: 'Abonements', value: 'abonements' }, { title: 'Price categories', value: 'price_category' }, { title: 'User Data', value: 'person_data' }, { title: 'Roles', value: 'roles' }];

export const list = [1, 2, 3];

export const reportList = [{ title: 'Sales of services', amount: 1248000 }, { title: 'Goods and abonements sold', amount: 350 }, { title: 'Abonements sold', amount: 150 }, { title: 'Prepayment amount', amount: 650000 }, { title: 'Quontity of sold services', amount: 45 }];

const getOnlyRoutes = (arr) => {
    // Returns an array that looks like this:
    // [
    //   {
    //     params: {
    //       id: 'object'
    //     }
    //   },
    //   {
    //     params: {
    //       id: 'service'
    //     }
    //   }
    // ]

    //   [ { params: { id: 'pre-rendering' } }, { params: { id: 'ssg-ssr' } } ]

    const routes = arr.map((item) => {
        const obj = {
            params: {
                id: item.value,
            },
        };

        return obj;
    });
    return routes;
};

const findFormData = (id, arr) => {
    const obj = arr.find((item) => item.value === id);
    return obj;
};

export default { getOnlyRoutes, findFormData };
