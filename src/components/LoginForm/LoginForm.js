import React from 'react';
import TextField from '@material-ui/core/TextField';

const Form = (props) => (
    <TextField
        type={props.type}
        variant="standard"
        margin="normal"
        required={props.required}
        fullWidth={props.fullWidth}
        value={props.value}
        label={props.label}
        name={props.name}
        autoFocus={props.autoFocus}
        onChange={props.onChange}
    />
);

export default Form;
