import React from 'react';
import styled from 'styled-components';
// import { Chart } from 'react-google-charts';
import { PieChart } from 'react-minimal-pie-chart';

const MyPichart = ({ arr }) => {
  let data = [];
  let initialValue = 0.1;
  arr.reduce((acc, currentValue) => {
    let randomColor = "#000000".replace(/0/g, function () {
      return (~~(Math.random() * 16)).toString(16);
    });
    let color = `rgba(3, 169, 244,${1 - acc})`;
    let insert = {
      color: color,
      title: currentValue.title,
      value: currentValue.value,
    };
    data.push(insert);
    return acc + 0.1
  }, initialValue);
  return (
    <Container>
      <Title>Statistics</Title>
      <SecondTitle>150</SecondTitle>
      <ChartWrapper>
        <PieChart
          data={data}
          animation
          animationDuration={500}
          animationEasing="ease-out"
          label={({ dataEntry }) => `${Math.round(dataEntry.percentage)} %`}
          labelPosition={105}
          labelStyle={{ fontSize: '0.375rem', fill: '#03a9f4' }}
        // viewBoxSize={[100, 100]}
        />;
            </ChartWrapper>
    </Container>
  );
};

export default MyPichart;

const Container = styled.div`
  background-color: #fff;
  height: 100%;
  min-width: 300px;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  border: 1px solid gray;
`;

const Title = styled.h1`
  margin: 0;
  text-align: center;
  font-size: 2rem;
`;

const SecondTitle = styled.h2`
  color: gray;
  margin: 1rem;
`;

const ChartWrapper = styled.div`
  width: 250px;
`;