import { SAVE_IMAGE } from '../actions/actions';

const initialState = {
    img: null,
};

const orderReducer = (state = initialState, action) => {
    switch (action.type) {
        case SAVE_IMAGE:
            return { ...state, img: action.img };
        default:
            return { ...state };
    }
};

export default orderReducer;
