import { GET_ALL_CATEGORY_ID, GET_CATEGORY_FORM_DATA, GET_MANAGE_LIST } from '../actions/actions';

const initialState = {
    manageProductsList: [],
    categoryIds: [],
    formData: {},
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_MANAGE_LIST:
            return { ...state, manageProductsList: action.manageProductsList };
        case GET_ALL_CATEGORY_ID:
            return { ...state, categoryIds: action.categoryIds };
        case GET_CATEGORY_FORM_DATA:
            return { ...state, formData: action.formData };
        default:
            return { ...state };
    }
};

export default reducer;
