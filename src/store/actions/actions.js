import util from '../../components/helper';

// Action Types
export const GET_MANAGE_LIST = 'GET_MANAGE_LIST';
export const GET_ALL_CATEGORY_ID = 'GET_ALL_CATEGORY_ID';
export const GET_CATEGORY_FORM_DATA = 'GET_CATEGORY_FORM_DATA';
export const SAVE_IMAGE = 'SAVE_IMAGE';

// Action Creator
export const getAllCategoryId = (ids) => ({
    type: GET_ALL_CATEGORY_ID, categoryIds: util.getOnlyRoutes(ids),
});

export const getManagedList = (data) => ({
    type: GET_MANAGE_LIST, manageProductsList: data,
});

export const getManageListAndCategoryId = (data) => (dispatch) => {
    dispatch(getAllCategoryId(data));
    dispatch(getManagedList(data));
};

export const saveImage = (img) => ({ type: SAVE_IMAGE, img });
