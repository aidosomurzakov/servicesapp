import { push } from "connected-react-router";
import axios from "../../axios-api";

import { NotificationManager } from "react-notifications";

export const LOGIN_USER_ERROR = 'LOGIN_USER_ERROR';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGOUT_USER = 'LOGOUT_USER';
export const REGISTER_USER_ERROR = 'REGISTER_USER_ERROR';
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';

const registerUserSuccess = () => {
    return { type: REGISTER_USER_SUCCESS };
};

const registerUserError = error => {
    return { type: REGISTER_USER_ERROR, error };
};

export const registerUser = userData => {
    return dispatch => {
        axios.post("/users", userData)
            .then(
                response => {
                    dispatch(registerUserSuccess());
                    dispatch(push("/"));
                },
                error => {
                    if (error.response && error.response.data) {
                        dispatch(registerUserError(error.response.data));
                    } else {
                        dispatch(registerUserError({ global: "No internet connection" }));
                    }
                }
            )
    }
};

const loginUserSuccess = (user) => {
    return { type: LOGIN_USER_SUCCESS, user };
};
const loginUserError = (error) => {
    return { type: LOGIN_USER_ERROR, error };
};

export const loginUser = userData => {
    return dispatch => {
        axios.post("/sign-in", userData)
            .then(
                response => {
                    dispatch(loginUserSuccess(response.data));
                    dispatch(push("/manage-panel"));
                },
                error => {
                    if (error.response && error.response.data) {
                        dispatch(loginUserError(error.response.data));
                    } else {
                        dispatch(loginUserError({ global: "No internet connection" }));
                    }
                }
            )
    }
};

export const logoutUser = () => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {
            Authorization: token
        };
        axios.delete("/users/sessions", { headers }).then(response => {
            dispatch({ type: LOGOUT_USER });
            NotificationManager.success(response.data.message);
            dispatch(push("/"));
        });
    }
};

export const facebookLogin = (data) => {
    return dispatch => {
        axios.post("/users/facebookLogin", data).then(responce => {
            dispatch(loginUserSuccess(responce.data));
            dispatch(push("/"));
            NotificationManager.success("Logged in with facebook");

        }, error => {
            dispatch(loginUserError(error.response.data));
        })
    };
};
