import React from 'react';
import styled from 'styled-components';

import { makeStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';

import CardImage from '../../components/CardImage/CardIImage';
import Table from '../../components/Table/Table';
import AutoComplete from '../../components/AutoComplete/AutoComplete';

const useStyles = makeStyles((theme) => ({
    btnColor: {
        '&:hover': {
            backgroundColor: '#0071a5'
        },
        backgroundColor: '#03a9f4',
        color: '#fff',
        maxWidth: '27%',
        padding: '11px 16px'

    },
}));


const List = ({ match }) => {
    const classes = useStyles();
    const {
        params: { serviceId },
    } = match;
    const cards = [{ title: 'Title 1', img: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__340.jpg' }, { title: 'Title 2', img: 'https://cdn.pixabay.com/photo/2015/02/24/15/41/dog-647528__340.jpg' }, { title: 'Title 3', img: 'https://s.yimg.com/uu/api/res/1.2/DdytqdFTgtQuxVrHLDdmjQ--~B/aD03MTY7dz0xMDgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://media-mbst-pub-ue1.s3.amazonaws.com/creatr-uploaded-images/2019-11/7b5b5330-112b-11ea-a77f-7c019be7ecae' }, { title: 'Title 3213451', img: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__340.jpg' },]
    const abonements = [{ title: 'Абонемент 1' }, { title: 'Абонемент 2', }, { title: 'Абонемент 3', },]
    return (
        <MainContainer>
            {serviceId === 'object' ?
                <>
                    <Title>Список обьектов</Title>
                    <Button variant="contained" className={classes.btnColor}>
                        Сохранить
                        </Button>
                    <AutoComplete arr={cards} />
                    <ContainersWrapper>
                        {cards.map(item => <CardImage title={item.title} avatar={item.img} serviceName={serviceId} />)}
                    </ContainersWrapper>
                </>
                : null}
            {serviceId === 'abonements' ?
                <>
                    <Title>Список обьектов</Title>
                    <Button variant="contained" className={classes.btnColor}>
                        Сохранить
                        </Button>
                    <AutoComplete arr={abonements} />
                    <Table serviceName={serviceId} />
                </>

                : null}
            {serviceId === 'agents' ?
                <>
                    <Title>Список Агентов</Title>
                    <Button variant="contained" className={classes.btnColor}>
                        Сохранить
                        </Button>
                    <AutoComplete arr={cards} />
                    <ContainersWrapper>
                        {cards.map(item => <CardImage title={item.title} avatar={item.img} serviceName={serviceId} />)}
                    </ContainersWrapper>
                </>
                : null}
            {serviceId === 'club_cards' ?
                <>
                    <Title>Список Клубных Карт</Title>
                    <Button variant="contained" className={classes.btnColor}>
                        Сохранить
                        </Button>
                    <AutoComplete arr={abonements} />
                    <Table serviceName={serviceId} />
                </>
                : null}
            {serviceId === 'person_data' ?
                <>
                    <Title>Список Пользователей</Title>
                    <Button variant="contained" className={classes.btnColor}>
                        Сохранить
                        </Button>
                    <AutoComplete arr={abonements} />
                    <Table serviceName={serviceId} />
                </>
                : null}
            {serviceId === 'roles' ?
                <>
                    <Title>Список Пользователей</Title>
                    <Button variant="contained" className={classes.btnColor}>
                        Сохранить
                        </Button>
                    <AutoComplete arr={abonements} />
                    <Table serviceName={serviceId} />
                </>
                : null}
            {serviceId === 'service' ?
                <>
                    <Title>Список Пользователей</Title>
                    <Button variant="contained" className={classes.btnColor}>
                        Сохранить
                        </Button>
                    <AutoComplete arr={abonements} />
                    <Table />
                </>
                : null}
            {serviceId === 'price_category' ?
                <>
                    <Title>Список Пользователей</Title>
                    <Button variant="contained" className={classes.btnColor}>
                        Сохранить
                        </Button>
                    <AutoComplete arr={abonements} />
                    <Table serviceName={serviceId} />
                </>
                : null}

        </MainContainer>
    )
}

export default List;

const Title = styled.h1`
  font-size: 1.9375rem;
  color: #666d73;
  margin-top: 1rem;

`;

const MainContainer = styled.div`
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  max-width: 1024px;
  div {
      margin-left:0;
  }
`;

const ContainersWrapper = styled.div`
  margin: 0 auto;
  display: grid;
  grid-template-columns: auto auto auto;
  padding: 10px;
  grid-gap: 1.5625rem;
  max-width: 65rem;
  box-sizing: border-box;
  padding: 2rem 0;
`;