import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { connect } from "react-redux";
import { NavLink as RouterNavLink } from 'react-router-dom';

import {
    NavLink,
} from "reactstrap";

import { loginUser } from "../../store/actions/usersActions";

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

import FormElement from '../../components/LoginForm/LoginForm';

const Login = ({ onLoginUser }) => {
    const [userInfo, setUserInfo] = useState({ email: '', password: '' });

    const inputChangeHandler = e => {
        const { name, value } = e.target;
        setUserInfo(prevState => ({
            ...prevState,
            [name]: value
        }))
    };
    const onSubmitHandler = e => {
        e.preventDefault();
        onLoginUser(userInfo)
    };
    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <FormWrapper>
                <Avatar>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    <CustomNavlink tag={RouterNavLink} to="/manage-panel" exact>While no login service just link -</CustomNavlink>
                </Typography>
                <form
                    noValidate
                    onSubmit={onSubmitHandler}
                >
                    <FormElement
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        label="Email"
                        name="email"
                        autoFocus
                        type="email"
                        value={userInfo.email}
                        onChange={inputChangeHandler}
                    />
                    <FormElement
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        label="Password"
                        name="password"
                        type="password"
                        value={userInfo.password}
                        onChange={inputChangeHandler}
                    />
                    <CustomButton
                        type="submit"
                        fullWidth
                        variant="outlined"
                        color="primary"
                    >
                        Sign In
          </CustomButton>
                </form>
            </FormWrapper>
        </Container>

    );
};

const mapDispatchToProps = dispatch => {
    return {
        onLoginUser: (userData) => dispatch(loginUser(userData)),
    };
};

export default connect(null, mapDispatchToProps)(Login);

const CustomButton = styled(Button)`
  margin-top: 2rem !important; 
`;

const FormWrapper = styled.div`
  a {
      color: black !important;
  }
`;

const CustomNavlink = styled(NavLink)`
    background-color: inherit;
    color: white;
    padding: 0;
`;
