import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import styled from 'styled-components';
import media from 'styled-media-query';

// import wrapper from '../redux/store';

import Container from '../../components/Container/Container';
import Card from '../../components/Card/Card';
import Circle from '../../components/Circle/Circle';
import PieChart from '../../components/PieChart/PieChart';
import Accordion from '../../components/Accordion/Accordion';
import RadioButton from '../../components/Ui/RadioButton/RadioButton';
import DatePicker from '../../components/DatePicker/DatePicker';
import Tabs from '../../components/Tabs/Tabs';
import { manageList, reportList } from '../../components/helper';

import { getManageListAndCategoryId } from '../../store/actions/actions';

const Panel = ({ onGetManageListAndCategoryId, manageProductsList }) => {
  useEffect(() => {
    onGetManageListAndCategoryId(manageList);
  }, [onGetManageListAndCategoryId]);

  const pieChartData = [
    { title: 'Agent 1', value: 21, color: '#03a9f4' },
    { title: 'Agent 2', value: 23, color: 'red' },
    { title: 'Agent 3', value: 50, color: 'green' },
    { title: 'Agent 3', value: 200, color: 'blue' },
  ];

  const tabs = ["today", "yesterday", "choose period"]
  return (
    <>
      <MainContainer>
        <Title>Management</Title>
        <ContainersWrapper>
          {manageProductsList.map((item, index) => <Card key={index} title={item.title} type="service" serviceName={item.value} />)}
        </ContainersWrapper>
        <SecondSection>
          <CustomContainer>
            <Title>Report</Title>
            <p>Choose period:</p>
            <Tabs items={tabs} centered={true} maxWidth="80%" />
            <ContainersWrapper>
              {reportList.map((item, index) => <Card key={index} item={item} type="report" />)}
            </ContainersWrapper>
            <DiagreammWrapper>
              <div className="flex-col pie-chart">
                <PieChart arr={pieChartData} />
              </div>
              <div className="flex-col">
                <Circle percent={20} number={1} />
                <Circle percent={87} number={13} />
              </div>
              <div className="flex-col">
                <Circle percent={30} number={25} />
                <Circle percent={47} number={7} />
              </div>
            </DiagreammWrapper>
          </CustomContainer>
        </SecondSection>
        <CustomContainer>
          <TitleTwo>Available reports</TitleTwo>
          <Accordion />
          <RadioButton labels={["Current", "For period"]} />
          <DatePicker title={'Choose date'} withoutTime />
        </CustomContainer>
      </MainContainer>

    </>
  );

}

const mapStateToProps = (state) => ({
  manageProductsList: state.mainReducer.manageProductsList,
});

const mapDispatchToProps = dispatch => {
  return {
    onGetManageListAndCategoryId: (list) => dispatch(getManageListAndCategoryId(list))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Panel);
// export default Panel;

const MainContainer = styled.div`
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const ContainersWrapper = styled.div`
  margin: 0 auto;
  display: grid;
  grid-template-columns: auto auto auto;
  padding: 10px;
  grid-gap: 1.5625rem;
  max-width: 65rem;
  box-sizing: border-box;
  padding: 2rem 0;

`;

const DiagreammWrapper = styled.div`
  margin: 3rem auto;
  display: flex;

  svg {
    overflow: visible;
  }
 
  .flex-col {
    min-height: 650px;

    ${media.lessThan('1366px')`
      min-height: auto;
    `}

    margin: 5px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex: 1;
    flex-direction: column;
    padding: 10px;
    box-sizing: border-box;
  }
  
`;

const SecondSection = styled.div`
  background-color: #dfe6f9;
  width: 100%;
  p {
    color: #948f8f;
  }
`;

const Title = styled.h1`
  color: #948f8f;
`;

const TitleTwo = styled.h2`
  color: #03a9f4;
  margin-bottom: 2rem;
`;

const CustomContainer = styled(Container)`
  margin-top: 1rem;
  box-sizing: border-box;
  padding: 3rem 0;
  ${Title} {
    text-align: center;
  }
  ${ContainersWrapper} {
    padding: 10px 0;
  }
`;
