import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { Link } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';

import Form from '../../components/Ui/Form/Form';
import LogoPhoto from '../../components/LogoPhoto/LogoPhoto';
import RadioButton from '../../components/Ui/RadioButton/RadioButton';
import DatePicker from '../../components/DatePicker/DatePicker';
import Switch from '../../components/Ui/Switch/Switch';
import Characteristics from '../../components/Characteristics/Characteristics';
import Checkbox from '../../components/Ui/Checkbox/Checkbox';
import SearchFilter from '../../components/SearchFilter/SearchFilter';
import Modal from '../Modal/Modal';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(2),
        },
    },
    btnColor: {
        '&:hover': {
            backgroundColor: '#0071a5'
        },
        backgroundColor: '#03a9f4',
        color: '#fff'
    },
    btnBorderColor: {
        borderColor: '#03a9f4',
        color: '#03a9f4 '
    }
}));

const CreateService = ({ match }) => {
    const classes = useStyles();
    const {
        params: { serviceId },
    } = match;
    useEffect(() => {
    }, [serviceId]);
    const labels = ['Urgent', 'By time', 'According to timer'];
    const characteristics = ['Charachteristics 1', 'Charachteristics 2', 'Charachteristics 3'];
    const timeCharacteristics = [{ title: 'Charachteristics 1', from: '02.07.2020', to: '15.07.2020' }, { title: 'Charachteristics 2', from: '16.07.2020', to: '30.07.2020' }, { title: 'Charachteristics 3', from: '01.08.2020', to: '15.08.2020' }];
    return (
        <Container>
            {serviceId === 'object' ? (
                <div>
                    <Title>Create new objects</Title>
                    <Form title="Object name" oneCollumn />
                    <Form title="Service description" defaultValue="Default Value" rows={4} multiline oneCollumn />
                    <Form title="Address" secondTitle="Город/Область" twoCollumn select />
                    <Form title="Post code" oneCollumn size="25ch" />
                    <Form title="Cell phone" oneCollumn size="25ch" />
                    <Form title="Email" oneCollumn size="25ch" />
                    <SecondTitle>Реквизиты</SecondTitle>
                    <Form title="Name of entity" oneCollumn />
                    <Form title="Document number" oneCollumn />
                    <Form title="Passport number" oneCollumn />
                    <Form title="Account number" oneCollumn />
                    <LogoPhoto />
                    <div className={classes.root}>
                        <Button variant="contained" className={classes.btnColor}>
                            Save
                        </Button>
                        <Button variant="outlined" className={classes.btnBorderColor}>
                            <Link to="/manage-panel">Cancel</Link>
                        </Button>
                    </div>
                    {/* <Modal /> */}

                </div>
            ) : null}
            {serviceId === 'agents' ? (
                <div>
                    <Title>Create new Agent</Title>
                    <Form title="Наименование агента" oneCollumn />
                    <Form title="Описание услуг агента" defaultValue="Default Value" rows={4} multiline oneCollumn />
                    <Form title="Адресс" secondTitle="Город/Область" twoCollumn select />
                    <Form title="Почтовый индекс" oneCollumn size="25ch" />
                    <Form title="Телефон" oneCollumn size="25ch" />
                    <Form title="Email" oneCollumn size="25ch" />
                    <SecondTitle>Реквизиты</SecondTitle>
                    <Form title="Наименование юр лица" oneCollumn />
                    <Form title="БИН" oneCollumn />
                    <Form title="БИК" oneCollumn />
                    <Form title="Расчетный счет (IBAN)" oneCollumn />
                    <LogoPhoto />
                    <Characteristics title={'Общие характеристики'} characteristics={characteristics} />
                    <Form oneCollumn select />
                    <div className={classes.root}>
                        <Button variant="contained" className={classes.btnColor}>
                            Сохранить
                        </Button>
                        <Button variant="outlined" className={classes.btnBorderColor}>
                            <Link to="/manage-panel">Отменить</Link>
                        </Button>
                    </div>

                </div>
            ) : null}
            {serviceId === 'service' ? (
                <div>
                    <Title>Создание Услуги</Title>
                    <Form title="Наименование услуги" oneCollumn />
                    <Form title="Администрирование услуги" oneCollumn select />
                    <RadioButton title="Тип услуги" serivce={true} labels={labels} />
                    <DatePicker title={'Выберите даты и время периода действия услуги'} />
                    <Switch service />
                    <Form title="Наименование услуги" conditional select title={"Класс услуги"} conditionalText={"или добавить новый класс"} size="25ch" />
                    <Form title="Подкласс услуги" conditional select title={"Подкласс услуги"} conditionalText={"или добавить новый подкласс"} size="25ch" />
                    <Characteristics title={'Общие характеристики'} characteristics={characteristics} />
                    <Characteristics byPeriod title={'Характеристики по периоду действия улсуги'} characteristics={timeCharacteristics} />
                    <RadioButton title="Условие возврата билета" serivce={true} labels={labels} />
                    <div className={classes.root}>
                        <Button variant="contained" className={classes.btnColor}>
                            Сохранить
                        </Button>
                        <Button variant="outlined" className={classes.btnBorderColor}>
                            <Link to="/manage-panel">Отменить</Link>
                        </Button>
                    </div>
                </div>
            ) : null}
            {serviceId === 'club_cards' ? (
                <div>
                    <Title>Создание Клубных карт</Title>
                    <Form title="Тип Карты" oneCollumn />
                    <Form title="Номер Карты" oneCollumn />
                    <Form title="ID карты" oneCollumn />
                    <Form title="Ценовая категория" oneCollumn select />
                    <div className={classes.root}>
                        <Button variant="contained" className={classes.btnColor}>
                            Сохранить
                        </Button>
                        <Button variant="outlined" className={classes.btnBorderColor}>
                            <Link to="/manage-panel">Отменить</Link>
                        </Button>
                    </div>
                </div>
            ) : null}
            {serviceId === 'schedule' ? (
                <div>
                    <Title>Создание Расписания</Title>
                    <Form title="Наименование объекта" oneCollumn />
                    <DatePicker title={'Выберите даты и время периода расписания работы'} />
                    <div className={classes.root}>
                        <Button variant="contained" className={classes.btnColor}>
                            Сохранить
                        </Button>
                        <Button variant="outlined" className={classes.btnBorderColor}>
                            <Link to="/manage-panel">Отменить</Link>
                        </Button>
                    </div>
                </div>
            ) : null}
            {serviceId === 'abonements' ? (
                <div>
                    <Title>Создание Абонементов</Title>
                    <Form title="Наименование абонемента" oneCollumn />
                    <Form title="Администратор абонемента" oneCollumn select />
                    <SecondTitle>Период действия</SecondTitle>
                    <Form title="Срок Действия" abonement period title={"Срок Действия"} conditionalText={"дня месяца или по истечении"} size="10ch" />
                    <Form title="Срок Действия" abonement startPeriod title={"Начало действия"} conditionalText={"дня месяца или"} size="10ch" />
                    <Form title="Наименование услуги" abonement endPeriod title={"Завершения действия"} conditionalText={"дня месяца или по истечении"} size="10ch" />
                    <DatePicker title={'Выберите даты периода реализации абонмента'} withoutTime />
                    <SecondTitle>Ограничения</SecondTitle>
                    <Switch abonements title={'Категории пользователей'} />
                    <Checkbox hourCheckbox title={"Ограничение действия абонемента"} labels={[{ title: 'Без ограничений', value: 'unlim' }, { title: 'По времени', value: 'lim' }]} />
                    <Checkbox weekCheckbox />
                    <Checkbox byPeriodAttendance />
                    <Checkbox byAttendance />
                    <div className={classes.root}>
                        <Button variant="contained" className={classes.btnColor}>
                            Сохранить
                        </Button>
                        <Button variant="outlined" className={classes.btnBorderColor}>
                            <Link to="/manage-panel">Отменить</Link>
                        </Button>
                    </div>
                </div>
            ) : null}
            {serviceId === 'price_category' ? (
                <div>
                    <Title>Создание Ценовых Категории</Title>
                    <SearchFilter />
                    <div className={classes.root}>
                        <Button variant="contained" className={classes.btnColor}>
                            Сохранить
                        </Button>
                        <Button variant="outlined" className={classes.btnBorderColor}>
                            <Link to="/manage-panel">Отменить</Link>
                        </Button>>
                    </div>
                </div>
            ) : null}
            {serviceId === 'person_data' ? (
                <div>
                    <Title>Создание Персональных Данных</Title>
                    <Form title="ИИН" oneCollumn />
                    <Form title="Фамилия" secondTitle="Имя" twoCollumn />
                    <Form title="Мобильный телефон" oneCollumn size="25ch" />
                    <Form title="Ценовая категория" oneCollumn select />
                    <SecondTitle>UID</SecondTitle>
                    <Form title="Тип носителя для считывания ID" conditional select conditionalText={"или введите вручную"} size="52ch" />
                    <Form title="ID" oneCollumn />
                    <SecondTitle>Услуги и абонементы</SecondTitle>
                    <Characteristics byPeriod title={'Достпуные услуги'} characteristics={timeCharacteristics} />
                    <Form title="Доступные услуги" oneCollumn select />
                    <div className={classes.root}>
                        <Button variant="contained" className={classes.btnColor}>
                            Сохранить
                        </Button>
                        <Button variant="outlined" className={classes.btnBorderColor}>
                            <Link to="/manage-panel">Отменить</Link>
                        </Button>
                    </div>
                </div>
            ) : null}
            {serviceId === 'roles' ? (
                <div>
                    <Title>Создание Ролей</Title>
                    <Form title="Наименование услуги" oneCollumn />
                    <Form title="Администрирование услуги" oneCollumn select />
                    <RadioButton title="Тип услуги" serivce={true} labels={labels} />
                    <DatePicker />
                    <Switch />
                    <Form title="Наименование услуги" conditional select title={"Класс услуги"} conditionalText={"или добавить новый класс"} size="25ch" />
                    <Form title="Подкласс услуги" conditional select title={"Подкласс услуги"} conditionalText={"или добавить новый подкласс"} size="25ch" />
                    <Characteristics title={'Общие характеристики'} characteristics={characteristics} />
                    <Characteristics byPeriod title={'Характеристики по периоду действия улсуги'} characteristics={timeCharacteristics} />
                    <RadioButton title="Условие возврата билета" serivce={true} labels={labels} />
                    <div className={classes.root}>
                        <Button variant="contained" className={classes.btnColor}>
                            Сохранить
                        </Button>
                        <Button variant="outlined" className={classes.btnBorderColor}>
                            <Link to="/manage-panel">Отменить</Link>
                        </Button>
                    </div>
                </div>
            ) : null}

        </Container>
    );
}

// export default connect(mapStateToProps, null)(CreateElement);

export default CreateService;

const Container = styled.div`
    margin: 3rem 0 0 5rem;
`;

const Title = styled.h1`
    font-size: 1.9375rem;
    margin-left: 0.9375rem;
    color: #666d73;
`;

const SecondTitle = styled.h2`
    margin-left: 0.9375rem;
    color: #666d73;
    font-size: 1.5625rem;
`;

const ParagraphCharacteristic = styled.p`
    margin-left: 0.9375rem;
    color: gray;
    font-size: 1.25rem;
`;