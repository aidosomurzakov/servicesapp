const config = {
    apiURL: process.env.REACT_APP_API_KEY,
};

export default config;
